document.addEventListener('click', processClick)
document.addEventListener('focus', processFocus)
document.addEventListener('input', processInput)
document.addEventListener('keydown', processKeydown)

function processClick() {
    console.log('PROCESS: click')

    let selected = document.getSelection()
    console.log('selected', selected)

    // let x = selected.getRangeAt(0).startContainer
    // console.log('x', x)

    let y = getSelectionBoundaryElement(true)
    console.log('y', y)
}

function processKeydown() { console.log('PROCESS: keydown') }
function processInput() { console.log('PROCESS: input') }
function processFocus() { console.log('PROCESS: focus') }

function getSelectionBoundaryElement(isStart) {
    let range, sel, container
    if (document.selection) {
        range = document.selection.createRange()
        range.collapse(isStart)
        return range.parentElement()
    } else {
        sel = window.getSelection()
        if (sel.getRangeAt) {
            if (sel.rangeCount > 0) {
                range = sel.getRangeAt(0)
            }
        } else {
            // Old WebKit
            range = document.createRange()
            range.setStart(sel.anchorNode, sel.anchorOffset)
            range.setEnd(sel.focusNode, sel.focusOffset)
            // Handle the case when the selection was selected backwards (from the end to the start in the document)
            if (range.collapsed !== sel.isCollapsed) {
                range.setStart(sel.focusNode, sel.focusOffset)
                range.setEnd(sel.anchorNode, sel.anchorOffset)
            }
        }
        if (range) {
           container = range[isStart ? "startContainer" : "endContainer"]
           // Check if the container is a text node and return its parent if so
           return container.nodeType === 3 ? container.parentNode : container
        }
    }
}


