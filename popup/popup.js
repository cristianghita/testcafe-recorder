function setup() {
    noCanvas()

    let bgpage = chrome.extension.getBackgroundPage()
    updateButtons()

    startStopButton.mousePressed(() => {
        bgpage.isRecording = !bgpage.isRecording
        updateButtons()
    })
}

function updateButtons() {
    let bgpage = chrome.extension.getBackgroundPage()
    let startStopButton = select('#start-stop-record-button')

    startStopButton.html(bgpage.isRecording ? 'Stop' : 'Start')
}
